#!/bin/bash

SESSION=${USER}_webdev

tmux -2 new-session -d -s $SESSION

tmux new-window -t $SESSION:1 -n "build"
tmux split-window -h
tmux select-pane -t 0
tmux send-keys "cd frontend; ember build --watch" C-m
tmux select-pane -t 1
tmux send-keys "python app.py" C-m
tmux -2 attach-session -t $SESSION
