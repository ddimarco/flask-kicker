#!/bin/env python
import os
import math
import yaml
import datetime
import dateutil.parser

from flask import Flask, request, jsonify, send_from_directory
from flask_restful import Resource, Api
# note: this is deprecated
from flask_restful import reqparse
from flask.ext.sqlalchemy import SQLAlchemy

DB_PATH = 'kicker.db'

app = Flask(__name__)
app.debug = True
api = Api(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + DB_PATH
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class Player(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False, unique=True)
    stat_id = db.Column(db.Integer, db.ForeignKey('player_stat.id'), nullable=False)
    stat = db.relationship('PlayerStat', back_populates='player')

    def __repr__(self):
        return '<Player %s>' % self.name
    @property
    def to_json(self):
        return {
            'type': 'player',
            'id': self.id,
            'attributes': {
                'name': self.name,
                'points': self.stat.points
            },

            'relationships': {
                'statistics': {
                    'data': { 'id': self.id, 'type': 'statistic'}
                }
            }
        }

class PlayerStat(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    player = db.relationship('Player', uselist=False, back_populates='stat')
    points = db.Column(db.Integer, nullable=False)

class Team(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    player1 = db.Column(db.Integer, db.ForeignKey('player.id'), nullable=False)
    player2 = db.Column(db.Integer, db.ForeignKey('player.id'), nullable=True)
    stat_id = db.Column(db.Integer, db.ForeignKey('team_stat.id'), nullable=False)
    stat = db.relationship('TeamStat', back_populates='team')
    def __repr__(self):
        return '<Team %i, Players %s>' % (self.id,
                                          [self.player1, self.player2]
                                          if self.player2
                                          else [self.player1] )
    @property
    def to_json(self):
        return {
            'type': 'team',
            'id': self.id,
            # 'player1': self.player1,
            # 'player2': self.player2
            'attributes': {
                'points': self.stat.points
            },
            'relationships': {
                'player1': {
                    'data': { 'id': self.player1, 'type': 'player'}
                },
                'player2': {
                    'data': { 'id': self.player2, 'type': 'player'}
                },
                'statistics': {
                    'data': { 'id': self.id, 'type': 'teamstatistic'}
                }
            }
        }
class TeamStat(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    team = db.relationship('Team', uselist=False, back_populates='stat')
    points = db.Column(db.Integer, nullable=False)

class Match(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    team1 = db.Column(db.Integer, db.ForeignKey('team.id'), nullable=False)
    team2 = db.Column(db.Integer, db.ForeignKey('team.id'), nullable=False)
    goals_team1 = db.Column(db.Integer, nullable=False, autoincrement=False)
    goals_team2 = db.Column(db.Integer, nullable=False, autoincrement=False)
    points = db.Column(db.Integer, nullable=False, autoincrement=False)
    crawling = db.Column(db.Boolean, nullable=False)
    matchday_id = db.Column(db.Integer, db.ForeignKey('matchday.id'))
    matchday = db.relationship('MatchDay', backref='matches')

    @property
    def to_json(self):
        return {
            'type': 'match',
            'id': self.id,
            'attributes': {
                'goals1': self.goals_team1,
                'goals2': self.goals_team2,
                'points': self.points,
                'crawling': self.crawling,
            },
            'relationships': {
                'team1': {
                 'data': {'id': self.team1, 'type': 'team'}
                },
                'team2': {
                 'data': {'id': self.team2, 'type': 'team'}
                }
            },
            'matchday_id': self.matchday_id
        }

class MatchDay(db.Model):
    __tablename__ = 'matchday'
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date, nullable=False, unique=True)

    @property
    def to_json(self):
        return {
            'type': 'matchday',
            'id': self.id,
            'attributes': {
                'date': self.date.isoformat()
            },
            'relationships': {
            'matches': {
                'data': [m.to_json for m in self.matches]
            }
            }
        }

def find_player(name):
    return Player.query.filter(Player.name == p).first()

def playerids2team(ids):
    assert(len(ids) <= 2)
    assert(len(ids) > 0)
    p1 = ids[0]
    if len(ids) == 1:
        return Team.query.filter(Team.player1 == p1, Team.player2.is_(None)).first()
    p2 = ids[1]
    return Team.query.filter(db.or_(db.and_(Team.player1 == p1, Team.player2 == p2),
                                    db.and_(Team.player1 == p2, Team.player2 == p1))).first()

def playername2player(player_name):
    player_qry = Player.query.filter(Player.name == player_name).first()
    assert(player_qry)
    return player_qry

def add_team(t1, pointsdiff):
    if len(t1) == 1:
        p1 = t1[0].id
        t_qry = Team.query.filter(Team.player1 == p1, Team.player2.is_(None)).first()
        if not t_qry:
            print('adding single player team: %s' % t1)
            team = Team()
            team.player1 = t1[0].id
            db.session.add(team)
            tm_stat = TeamStat(team=team, points=1200+pointsdiff)
            db.session.add(tm_stat)
        else:
            print('single player team found: %s' % t_qry)
            t_qry.stat.points += pointsdiff
    elif len(t1) == 2:
        p1 = t1[0].id
        p2 = t1[1].id
        t_qry = Team.query.filter(db.or_(db.and_(Team.player1 == p1, Team.player2 == p2),
                                         db.and_(Team.player1 == p2, Team.player2 == p1))).first()
        if not t_qry:
            print('adding team: %s' % t1)
            team = Team()
            team.player1 = t1[0].id
            team.player2 = t1[1].id
            db.session.add(team)
            tm_stat = TeamStat(team=team, points=1200+pointsdiff)
            db.session.add(tm_stat)
        else:
            print ('team found: %s' % t_qry)
            t_qry.stat.points += pointsdiff

def import_matches(path):
    matchdays_all = {}
    for fn in os.listdir(path):
        with open('%s/%s' % (path, fn), 'r') as infile:
            day = yaml.load(infile)
        matchdays_all[day['date']] = day
    # create db data
    # players
    for day,md in matchdays_all.items():
        for m in md['matches']:
            for p in m['team1'] + m['team2']:
                win = p in m['team1']
                pointsdiff = m['difference'] if win else -m['difference']
                player_qry = Player.query.filter(Player.name == p).first()
                if not player_qry:
                    plr = Player(name=p)
                    db.session.add(plr)
                    plr_stats = PlayerStat(player=plr,points=1200+pointsdiff)
                    db.session.add(plr_stats)
                else:
                    # just update the points
                    player_qry.stat.points += pointsdiff
    # teams
    for day,md in matchdays_all.items():
        for m in md['matches']:
            t1 = [playername2player(p) for p in m['team1']]
            add_team(t1, m['difference'])
            t2 = [playername2player(p) for p in m['team2']]
            add_team(t2, -m['difference'])
    # matchdays & matches
    for day,md in matchdays_all.items():
        [day, month, year] = md['date'].split('.')
        date = datetime.date(int(year), int(month), int(day))
        md_qry = MatchDay.query.filter(MatchDay.date == date).first()
        if not md_qry:
            print('adding matchday: %s' % md)
            matchday = MatchDay(date=date)
            for m in md['matches']:
                t1 = playerids2team([playername2player(p).id for p in m['team1']])
                t2 = playerids2team([playername2player(p).id for p in m['team2']])
                [goals_t1, goals_t2] = map(int, m['score'].split(':'))
                match = Match(crawling=m['crawling'],
                              team1=t1.id, team2=t2.id,
                              goals_team1=goals_t1, goals_team2=goals_t2,
                              points=m['difference'])
                matchday.matches.append(match)
            db.session.add(matchday)
    db.session.commit()

##########################################################

class MatchDaysResource(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('page', type=int)
        parser.add_argument('per_page', type=int)
        args = parser.parse_args()
        print('url: %s' % request.url)
        page_number = args.get('page')
        page_size = args.get('per_page')
        if not page_number:
            page_number = 1
        if not page_size:
            page_size = 7
        start_idx = (page_number - 1)*page_size
        matchdays = MatchDay.query.order_by(MatchDay.date.desc())[start_idx:start_idx+page_size]
        total_pages = math.ceil(MatchDay.query.count() / page_size)
        print('total: %s' % total_pages)
        res = {'data': [md.to_json for md in matchdays],
                'meta': {
                    'total_pages': total_pages,
                },
                'links': {
                    'self': request.url,
                    'first': '{baseurl}?page[number]={pn}&page[size]={ps}'.format(
                        baseurl=request.base_url, pn=1, ps=page_size),
                    'last': '{baseurl}?page[number]={pn}&page[size]={ps}'.format(
                        baseurl=request.base_url, pn=total_pages-1, ps=page_size),
                    'prev': '{baseurl}?page[number]={pn}&page[size]={ps}'.format(
                        baseurl=request.base_url, pn=(page_number-1), ps=page_size),
                    'next': '{baseurl}?page[number]={pn}&page[size]={ps}'.format(
                        baseurl=request.base_url, pn=(page_number+1), ps=page_size)
                }
        }
        return res
class MatchDayResource(Resource):
    def get(self, id):
        md = MatchDay.query.get(id)
        return {'data': md.to_json}
class MatchDayByDateResource(Resource):
    def get(self, date_iso):
        date = dateutil.parser.parse(date_iso)
        md = MatchDay.query.filter(MatchDay.date == date.date()).first()
        return {'data': md.to_json}

class MatchesResource(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('filter[id]')
        args = parser.parse_args()
        filter_ids = []
        if args.get('filter[id]'):
            filter_ids = list(map(lambda s: int(s),
                                  filter(lambda x: x, args.get('filter[id]').split(','))))
        if len(filter_ids) == 0:
            matches = Match.query.all()
        else:
            matches = Match.query.filter(Match.id.in_(filter_ids)).all()
        print('returning %i matches' % len(matches))
        return {'data': [m.to_json for m in matches]}
class MatchResource(Resource):
    def get(self, id):
        match = Match.query.get(id)
        return {'data': match.to_json}

class TeamsResource(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('filter[id]')
        args = parser.parse_args()
        filter_ids = []
        if args.get('filter[id]'):
            filter_ids = list(map(lambda s: int(s),
                                  filter(lambda x: x, args.get('filter[id]').split(','))))
        if len(filter_ids) == 0:
            teams = Team.query.join(TeamStat, Team.stat).order_by(TeamStat.points.desc()).all()
        else:
            teams = Team.query.join(TeamStat, Team.stat).filter(Team.id.in_(filter_ids)).order_by(TeamStat.points.desc()).all()
        print('returning %i teams' % len(teams))
        return {'data': [t.to_json for t in teams]}
class TeamResource(Resource):
    def get(self, id):
        team = Team.query.get(id)
        return {'data': team.to_json}

class PlayersResource(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('filter[id]')
        args = parser.parse_args()
        filter_ids = []
        if args.get('filter[id]'):
            filter_ids = list(map(lambda s: int(s),
                                  filter(lambda x: x, args.get('filter[id]').split(','))))
        if len(filter_ids) == 0:
            players = Player.query.join(PlayerStat, Player.stat).order_by(PlayerStat.points.desc()).all()
        else:
            players = Player.query.join(PlayerStat, Player.stat).filter(Player.id.in_(filter_ids)).order_by(PlayerStat.points.desc()).all()
        print('returning %i players' % len(players))
        return {'data': [p.to_json for p in players]}
class PlayerResource(Resource):
    def get(self, id):
        player = Player.query.get(id)
        return {'data': player.to_json}

class PlayerStatisticsResource(Resource):
    def get(self, id):
        # FIXME: keep this data in db table
        player = Player.query.get(id)
        teams = db.session.query(Team.id).filter(db.or_(Team.player1 == id, Team.player2 == id)).subquery()
        # team 1 is always the winning one
        won_matches = Match.query.filter(Match.team1.in_(teams)).all()
        lost_matches = Match.query.filter(Match.team2.in_(teams)).all()
        matches_played = len(won_matches) + len(lost_matches)

        made_crawl = Match.query.filter(Match.team1.in_(teams), Match.crawling).all()
        did_crawl = Match.query.filter(Match.team2.in_(teams), Match.crawling).all()

        points_sum = player.stat.points
        avg_points = float(points_sum-1200) / float(matches_played)

        all_matches = Match.query.filter(db.or_(Match.team1.in_(teams), Match.team2.in_(teams))).join(MatchDay, Match.matchday).order_by(MatchDay.date).all()

        won_ids = [m.id for m in won_matches]
        lost_ids = [m.id for m in lost_matches]
        points_hist = []
        points = 1200
        matchdays = []
        for m in all_matches:
            if m.id in won_ids:
                points += m.points
            elif m.id in lost_ids:
                points -= m.points
            else:
                raise RuntimeError('ERROR! you messed up!')
            points_hist.append(points)
            matchdays.append(m.matchday.date.isoformat())

        res = {'data': {
            'attributes': {
                'name': player.name,
                'matchesplayed': len(won_matches) + len(lost_matches),
                'matcheswon': len(won_matches),
                'matcheslost': len(lost_matches),
                'madecrawl': len(made_crawl),
                'didcrawl': len(did_crawl),
                'points': points_sum,
                'avgpoints': avg_points,
                'pointshist': points_hist, #[-100:],
                'matchdays': matchdays #[-100:]
            },
            'type': 'statistic',
            'id': id
        }
        }
        return res

class TeamStatisticsResource(Resource):
    def get(self, id):
        team = Team.query.get(id)

        won_matches = Match.query.filter(Match.team1 == team.id).all()
        lost_matches = Match.query.filter(Match.team2 == team.id).all()

        made_crawl = Match.query.filter(Match.team1 == team.id, Match.crawling).all()
        did_crawl = Match.query.filter(Match.team2 == team.id, Match.crawling).all()

        all_matches = Match.query.filter(db.or_(Match.team1 == team.id, Match.team2 == team.id)).join(MatchDay, Match.matchday).order_by(MatchDay.date).all()

        won_ids = [m.id for m in won_matches]
        lost_ids = [m.id for m in lost_matches]
        points_hist = []
        points = 1200
        matchdays = []
        for m in all_matches:
            if m.id in won_ids:
                points += m.points
            elif m.id in lost_ids:
                points -= m.points
            else:
                raise RuntimeError('ERROR! you messed up!')
            points_hist.append(points)
            matchdays.append(m.matchday.date.isoformat())

        res = {'data': {
            'attributes': {
                'matchesplayed': len(won_matches) + len(lost_matches),
                'matcheswon': len(won_matches),
                'matcheslost': len(lost_matches),
                'madecrawl': len(made_crawl),
                'didcrawl': len(did_crawl),

                'pointshist': points_hist,
                'matchdays': matchdays
            },
            'type': 'teamstatistic',
            'id': id
        }
        }
        return res


class PlayerByNameResource(Resource):
    def get(self, name):
        player = Player.query.filter(Player.name == name).first()
        return {'data': player.to_json}

api.add_resource(MatchDaysResource, '/api/matchdays', methods=['GET'])
api.add_resource(MatchDayResource, '/api/matchdays/<int:id>', methods=['GET'])
api.add_resource(MatchDayByDateResource, '/api/matchday/by-date/<string:date_iso>',
                 methods=['GET'])
api.add_resource(MatchesResource, '/api/matches', methods=['GET'])
api.add_resource(MatchResource, '/api/matches/<int:id>', methods=['GET'])
api.add_resource(TeamsResource, '/api/teams', methods=['GET'])
api.add_resource(TeamResource, '/api/teams/<int:id>', methods=['GET'])
api.add_resource(PlayersResource, '/api/players', methods=['GET'])
api.add_resource(PlayerResource, '/api/players/<int:id>', methods=['GET'])
api.add_resource(PlayerByNameResource, '/api/players/by-name/<string:name>', methods=['GET'])

api.add_resource(PlayerStatisticsResource, '/api/statistics/<int:id>', methods=['GET'])
api.add_resource(TeamStatisticsResource, '/api/teamstatistics/<int:id>', methods=['GET'])

##########################################################

@app.route("/")
def index():
    return send_from_directory(app.static_folder, 'index.html')

@app.route('/<path:path>')
def static_proxy(path):
  # send_static_file will guess the correct MIME type
  return app.send_static_file(path)

if __name__ == '__main__':
    need_reimport = not os.path.isfile(DB_PATH)
    db.create_all()
    if need_reimport:
        print('importing old data...')
        import_matches('/home/dm/code/kicker-scraper/data')
    #app.run(host='0.0.0.0')
    app.run()
