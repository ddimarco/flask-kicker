import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('players');
  this.route('player', {path: '/player/:id'});

  this.route('teams');
  this.route('team', {path: '/team/:id'});
});

export default Router;
