import Ember from 'ember';

export default Ember.Controller.extend({
    teamSort: ['points:desc'],
    sortedTeams: Ember.computed.sort('model', 'teamSort')
});
