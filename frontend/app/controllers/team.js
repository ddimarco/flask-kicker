import Ember from 'ember';

export default Ember.Controller.extend({
    matchesData: Ember.computed('model.statistics.matcheswon', function() {
        var won = this.get('model.statistics.matcheswon');
        var lost = this.get('model.statistics.matcheslost');
        if (!won) {
            won = 1;
        }
        if (!lost) {
            lost = 0;
        }
        var data = [
            {
                x: 'Matches Won',
                y: won
            },
            {
                x: 'Matches Lost',
                y: lost
            }
        ];
        return data;
    }),

    matchesDataOptions: {
        chart: {
            labelsOutside: true,
            title: 'Matches Statistics',
            showLegend: false
        },
        tooltip: {
            valueFormatter: function(d) {
                return d.toFixed(0);
            }
        }
    },

    matchesDataBeforeSetup: function(svgContainer /*, chart */) {
        svgContainer.attr({
            height: 300
        });
    },

    crawlData: Ember.computed('model.statistics.madecrawl', function() {
        var madecrawl = this.get('model.statistics.madecrawl');
        var didcrawl = this.get('model.statistics.didcrawl');
        if (!madecrawl) {
            madecrawl = 1;
        }
        if (!didcrawl) {
            didcrawl = 0;
        }
        var data = [
            {
                x: 'Made crawl',
                y: madecrawl
            },
            {
                x: 'Crawled',
                y: didcrawl
            }
        ];
        return data;
    }),

    crawlOptions: {
        chart: {
            donut: true,
            labelsOutside: true,
            title: 'Crawling Statistics',
            showLegend: false,
            padAngle: 0.02
        },

        tooltip: {
            valueFormatter: function(d) {
                return d.toFixed(0);
            }
        }
    },

    pointsHistOptions: {
        chart: {
            focusHeight: 100
        }
        // xAxis: {
        //     tickFormat(d) {
        //         return d3.time.format("%d.%m")(new Date(d));
        //     }
        // }
    },

    pointsHistBeforeSetup: function(svgContainer /*, chart */) {
        svgContainer.attr({
            height: 600
        });
    },

    pointsHistData: Ember.computed('model.statistics.pointshist', 'limitHistory', function() {
        var pointshist = this.get('model.statistics.pointshist');
        var matchdays = this.get('model.statistics.matchdays');
        if (!pointshist) {
            pointshist = [0];
        }
        if (!matchdays) {
            matchdays = [0];
        }
        if (this.limitHistory) {
            var lp = pointshist.length;
            pointshist = pointshist.slice(lp-100, lp);
            matchdays = matchdays.slice(lp-100, lp);
        }

        var vals = [];
        for(var i=0; i<matchdays.length; i++) {
            vals.push({
                x: i, //new Date(matchdays[i]),
                y: pointshist[i]
            });
        }

        var data = [{
            area: false,
            values: vals,
            key: 'Points History'
        }];
        return data;
    }),
    limitHistory: false,

    actions: {
        fullHistory() {
            console.log("full history clicked");
            this.set('limitHistory', false);
        },

        limitHistory() {
            console.log("limit history clicked");
            this.set('limitHistory', true);
        }
    }
});
