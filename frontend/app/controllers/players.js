import Ember from 'ember';

export default Ember.Controller.extend({
    playerSort: ['points:desc', 'name:desc'],
    sortedPlayers: Ember.computed.sort('model', 'playerSort')
});
