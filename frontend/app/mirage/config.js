export default function() {
    this.namespace = '/api';

    var matchdays = [{
        type: 'matchday',
        id: 0,
        attributes: {
            date: '2015-09-02'
        },
        relationships: {
            matches: {
                data: [
                    {
                    type: 'match',
                    id: 0
                    }
                ]
            }
        }
    }, {
        type: 'matchday',
        id: 1,
        attributes: {
            date: '2015-09-03'
        },
        relationships: {
            matches: {
                data: [
                    {
                        type: 'match',
                        id: 1
                    },
                    {
                        type: 'match',
                        id: 2
                    }
                ]
            }
        }
    }];

    this.get('/matchdays', function() {
        return {
            data: matchdays
        };
    });

    // this.get('/matchdays/:id', function(db, request) {
    //     var id = request.params.id;
    //     return matchdays[id];
    // });

    var matches = [{
        type: 'match',
        id: 0,
        attributes: {
            goals1: 6,
            goals2: 4,
            crawling: false
        },
        relationships: {
            team1: {
                data: {id: 0, type: 'team'}
            },
            team2: {
                data: {id: 1, type: 'team'}
            }
        }
    }, {
        type: 'match',
        id: 1,
        attributes: {
            goals1: 6,
            goals2: 1,
            crawling: false
        },
        relationships: {
            team1: {
                data: {id: 0, type: 'team'}
            },
            team2: {
                data: {id: 2, type: 'team'}
            }
        }
    }, {
        type: 'match',
        id: 2,
        attributes: {
            goals1: 6,
            goals2: 0,
            crawling: true
        },
        relationships: {
            team1: {
                data: {id: 0, type: 'team'}
            },
            team2: {
                data: {id: 3, type: 'team'}
            }
        }
    }];

    this.get('/matches', function() {
        return {
            data: matches
        };
    });

    this.get('/matches/:id', function(db, request) {
        var id = request.params.id;
        return {
            data: matches[id]
        };
    });


    var teams = [{
        type: 'team',
        id: 0,
        relationships: {
            player1: {
                data: {id: 0, type: 'player'}
            },
            player2: {
                data: {id: 1, type: 'player'}
            }
        }
    },{
        type: 'team',
        id: 1,
        relationships: {
            player1: {
                data: {id: 2, type: 'player'}
            },
            player2: {
                data: {id: 3, type: 'player'}
            }
        }
    },{
        type: 'team',
        id: 2,
        relationships: {
            player1: {
                data: {id: 0, type: 'player'}
            },
            player2: {
                data: {id: 3, type: 'player'}
            }
        }
    },{
        type: 'team',
        id: 3,
        relationships: {
            player1: {
                data: {id: 1, type: 'player'}
            },
            player2: {
                data: {id: 3, type: 'player'}
            }
        }
    }];

    this.get('/teams', function() {
        return {
            data: matches
        };
    });
    this.get('/teams/:id', function(db, request) {
        var id = request.params.id;
        return {
            data: teams[id]
        };
    });



    var players = [{
        type: 'player',
        id: 0,
        attributes: {
            name: 'Player1'
        }
    },{
        type: 'player',
        id: 1,
        attributes: {
            name: 'Player2'
        }
    }, {
        type: 'player',
        id: 2,
        attributes: {
            name: 'Player3'
        }
    }, {
        type: 'player',
        id: 3,
        attributes: {
            name: 'Player4'
        }
    }];

    this.get('/players', function() {
        return {
            data: players
        };
    });
    this.get('/players/:id', function(db, request) {
        var id = request.params.id;
        return {
            data: players[id]
        };
    });
}
