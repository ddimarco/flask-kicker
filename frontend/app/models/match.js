import DS from 'ember-data';

export default DS.Model.extend({
    team1: DS.belongsTo('team'),
    team2: DS.belongsTo('team'),
    goals1: DS.attr(),
    goals2: DS.attr(),
    crawling: DS.attr()
});
