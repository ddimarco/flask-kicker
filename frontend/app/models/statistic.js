import DS from 'ember-data';

export default DS.Model.extend({
    name: DS.attr('string'),
    matchesplayed: DS.attr(),
    matcheswon: DS.attr(),
    matcheslost: DS.attr(),
    madecrawl: DS.attr(),
    didcrawl: DS.attr(),
    points: DS.attr(),
    avgpoints: DS.attr(),
    pointshist: DS.attr(),
    matchdays: DS.attr()
});
