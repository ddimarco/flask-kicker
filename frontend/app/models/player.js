import DS from 'ember-data';

export default DS.Model.extend({
    name: DS.attr('string'),
    points: DS.attr('number'),

    statistics: DS.belongsTo('statistic')

    // matches_gauge_data: Ember.computed(function() {
    //     console.log('calculating matches gauge data for ' + this.id);
    //     //console.log(this.get('statistics'));
    //     var stats = this.get('statistics');
    //     console.log(stats);
    //     var data = {
    //         columns: [
    //             // ['data', 91.4]
    //             ['won', stats.matcheswon],
    //             //['lost', this.statistics.matcheslost]
    //         ],
    //         type: 'gauge'
    //     };
    //     console.log(data);
    //     return data;
    // })

});
