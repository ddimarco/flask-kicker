import DS from 'ember-data';

export default DS.Model.extend({
    player1: DS.belongsTo('player'),
    player2: DS.belongsTo('player'),
    points: DS.attr('number'),

    statistics: DS.belongsTo('teamstatistic')
});
