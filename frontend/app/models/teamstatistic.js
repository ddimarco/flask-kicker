import DS from 'ember-data';

export default DS.Model.extend({
    matchesplayed: DS.attr(),
    matcheswon: DS.attr(),
    matcheslost: DS.attr(),
    madecrawl: DS.attr(),
    didcrawl: DS.attr(),
    pointshist: DS.attr(),
    matchdays: DS.attr()
});
