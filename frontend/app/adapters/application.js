import JSONAPIAdapter from 'ember-data/adapters/json-api';

export default JSONAPIAdapter.extend({
    namespace: '/api',
    // combine find requests via filter[id]=...
    coalesceFindRequests: true
});
