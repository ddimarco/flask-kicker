import Ember from 'ember';

var monthNames = [
  "January", "February", "March",
  "April", "May", "June", "July",
  "August", "September", "October",
  "November", "December"
];

export function formatDate([date]/*, hash*/) {
    return date.getDate() + '. ' + monthNames[date.getMonth()] + ' ' + date.getFullYear();
}

export default Ember.Helper.helper(formatDate);
