import Ember from 'ember';
import RouteMixin from 'ember-cli-pagination/remote/route-mixin';

export default Ember.Route.extend(RouteMixin, {
    page: 1,
    perPage: 10,

    model(params) {
        return this.findPaged('matchday', params);
    }
});
